COMMAND=ghc
STATICCOMPOPT=-optl-static -optl-pthread
OPTIONS=
ARCH=${shell uname -p}
PROJECT=${shell basename `pwd`}
OUTFILE=run${PROJECT}_${ARCH}

all:	compile
#	echo ${ARCH}
#	echo ${PROJECT}

compile:
	@command -v ${COMMAND} >/dev/null 2>&1 || { echo >&2 "I require ${COMMAND} but it's not installed.  Aborting."; exit 1; }
	cd src/ ; ${COMMAND} ${OPTIONS} -o ${OUTFILE} Main.hs

install:
	cp src/${OUTFILE} dist/${OUTFILE} 

zip: clean
	cd ../ ; make -f ${PROJECT}/Makefile zipFromParent
	mv ../${PROJECT}.zip ./
	zip --delete ${PROJECT}.zip ${PROJECT}/.git/*
	zip --delete ${PROJECT}.zip ${PROJECT}/src_outdated/*

zipFromParent:
	zip -r ${PROJECT}.zip ${PROJECT}/

clean:
	rm ${PROJECT}.zip src/*.hi src/*.o src/run${PROJECT}_i386

installzip: install zip
	cp ${PROJECT}.zip ../../html/DOCS/

installhtml: installzip
	cp ${PROJECT}.zip ../../html/
	rm -rf ../../html/${PROJECT}/
	cd ../../html/ ; unzip ${PROJECT}.zip ; rm ${PROJECT}.zip

stats:
	wc -l src/*.hs html/*.php 

