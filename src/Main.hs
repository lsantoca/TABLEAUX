import Statements
--import Rules
import Tableaux
import qualified StatementsParse  as Parser
import StatementsShow

import System.IO
import System.Environment
import System.Exit hiding (die)
import Control.Exception


-- TODO :
-- Use System.Posix.IO to redirect stdErr to StdOut

formulaConstant = "-([](p | q) -> ([]p | []q))"

runOnFormula :: String -> IO ()  
runOnFormula formulaString =
  let
    fail :: SomeException -> IO Formula
    fail _ = hPutStrLn stderr "Could not parse the formula" >> die
  in
  do
    formula <- (let f = Parser.parseString formulaString in f `seq` return f)
      `catch` fail
    hPutStrLn stderr $ "Running the tableaux method for satisfiability on formula ** " ++ show formula  ++ " **\n"
    result <- return (formulaIsSatisfiable formula)
    hPutStr stderr "\n"
    if result then
      hPutStrLn stderr "\nFormula is satisfiable"
      else
      hPutStrLn stderr "\nFormula is unsatisfiable"
    exit 

runOnFile :: String -> IO ()
runOnFile path =
  do
    hPutStrLn stderr "Yet to be implemented"
  
main :: IO ()
main = do
  args <- getArgs
  parsedArgs <- parse args
  -- We need to redirect all the stderr onto the stdout
  case parsedArgs of
    Nil -> runOnFormula formulaConstant
    Formula formula -> runOnFormula formula
    Path path -> runOnFile path


    
data ParsedAnswer = Nil | Path String | Formula String 
parse :: [String] -> IO ParsedAnswer
parse [] = return Nil
parse ["-h"] = usage >> exit
parse ("--formula":formula:_) =return (Formula  formula)
parse (fileName:_)   = return (Path fileName)

usage :: IO ()
usage   = putStrLn $
  "Usage: runTableaux [-h] [--nointeractive] [file]\n"
  ++"\trun Tableaux algorithm on formula\n"
  ++"\tif no file is given, then Tableuax is run on the formula -([](p | q) -> ([]p | []q)"

exit    = exitWith ExitSuccess
die     = exitWith (ExitFailure 1)



{-
import Control.Applicative
import Options

data MainOptions = MainOptions
    { optMessage :: String
    , optQuiet :: Bool
    }

instance Options MainOptions where
    defineOptions = pure MainOptions
        <*> simpleOption "message" "Hello world!"
            "A message to show the user."
        <*> simpleOption "quiet" False
            "Whether to be quiet."

main :: IO ()
main = runCommand $ \opts args -> do
    if optQuiet opts
        then return ()
        else putStrLn (optMessage opts)
-}

