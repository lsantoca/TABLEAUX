module Statements (
  Variable(..),
  State(..),
  Formula(..),
  Statement(..),
  Statements,
  isLiteral,
  newState,
  incoherent
  )
  where

import Data.IORef
import System.IO.Unsafe




data Variable = V String deriving (Eq)
data State = S Int deriving (Eq)
data Formula =  Var Variable
                | And Formula Formula | Or Formula Formula
                | Impl Formula Formula | Neg Formula
                | Nec Formula deriving (Eq)

data Statement = Sat State Formula | Succ  State State deriving (Eq)
type Statements = [Statement]

isLiteral :: Statement -> Bool
isLiteral (Sat _ (Var _)) = True
isLiteral (Sat _ (Neg (Var _))) = True
isLiteral _ = False

oppositeLiterals :: Statement -> Statement -> Bool
oppositeLiterals (Sat s0 (Var n0)) (Sat s1 (Neg (Var n1))) =
  s0 == s1 && n0 == n1
oppositeLiterals (Sat s1 (Neg (Var n1))) (Sat s0 (Var n0)) =
  s0 == s1 && n0 == n1
oppositeLiterals _ _ = False

incoherent :: Statements -> Statements -> Bool
incoherent as bs = case bs of
  [] -> False
  c:cs -> if not (isLiteral c) then incoherent as cs
          else
            any (oppositeLiterals c) (as++cs)


{-# NOINLINE uniqueRef #-}
uniqueRef :: IORef Int
uniqueRef = unsafePerformIO $ newIORef 0

uniqueInt :: IO Int
uniqueInt = do
    n <- readIORef uniqueRef
    writeIORef uniqueRef (n+1)
    return n

newState :: IO State
newState =  do
  n <- uniqueInt
  return (S n)

