module StatementsParse where
import Statements

{-- Depuis https://wiki.haskell.org/Parsing_a_simple_imperative_language --}

--import System.IO
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token


languageDef =
   emptyDef { Token.commentStart    = "/*"
            , Token.commentEnd      = "*/"
            , Token.commentLine     = "//"
            , Token.identStart      = letter
            , Token.identLetter     = alphaNum
            , Token.reservedNames   = [ "&"
                                      , "|"
                                      , "->"
                                      , "[]"
                                      ]
            , Token.reservedOpNames = ["&", "|", "-", "[]", "->"]
            }

lexer = Token.makeTokenParser languageDef
identifier = Token.identifier lexer -- parses an identifier
reserved   = Token.reserved   lexer -- parses a reserved name
reservedOp = Token.reservedOp lexer -- parses an operator
parens     = Token.parens     lexer -- parses surrounding parenthesis:
                                     --   parens p
                                     -- takes care of the parenthesis and
                                     -- uses p to parse what's inside them
integer    = Token.integer    lexer -- parses an integer
semi       = Token.semi       lexer -- parses a semicolon
whiteSpace = Token.whiteSpace lexer -- parses whitespace

formulaParser :: Parser Formula
formulaParser = Token.whiteSpace lexer >> formula


{--
Following
https://www.cs.unm.edu/~mccune/mace4/manual/2009-11A/syntax.html#built_in
We have
-  []   &  ->  |

--}

operators = [
  [Prefix (reservedOp "-"   >> return (Neg))]
  ,
  [Prefix (reservedOp "[]"   >> return (Nec))]
  ,
  [Infix  (reservedOp "&"   >> return (And)) AssocLeft]
  ,
  [Infix  (reservedOp "->"   >> return (Impl     )) AssocLeft]
  ,
  [ Infix  (reservedOp "|"   >> return (Or  )) AssocLeft]
  ]
               
formula :: Parser Formula
formula = buildExpressionParser operators aFormula

aFormula = parens formula
  <|> liftM Var var

var :: Parser Variable
var = do
  ident <- Token.identifier lexer
  return (V ident)

parseString :: String -> Formula
parseString str =
  case parse formulaParser "" str of
    Left e  -> error $ show e
    Right r -> r
 
parseFile :: String -> IO Formula
parseFile file =
   do program  <- readFile file
      case parse formulaParser "" program of
        Left e  -> print e >> fail "parse error"
        Right r -> return r
