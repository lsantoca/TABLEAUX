module UniqueInt (uniqueInt)
       where

import Data.IORef

class OnceInit a where
   onceInit :: IO a
 
class OnceInit a => OnceIO a where
   runOnceIO :: IO a

newtype UniqueRef = UniqueRef (IORef Integer)
                       deriving OnceIO
 
instance OnceInit UniqueRef where
   onceInit = liftM UniqueRef (newIORef 0)
 
uniqueInt :: IO Integer
uniqueInt = do
   UniqueRef uniqueRef <- runOnceIO
   n <- readIORef uniqueRef
   writeIORef uniqueRef (n+1)
   return n
