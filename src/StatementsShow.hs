module StatementsShow where
import Statements
import Text.Printf


instance Show Variable where
  show (V string ) = string

instance Show State where
  show (S n) = "s_"++show n
  
instance Show Formula where
  show formula =
    let
      pprintBinFormula op sub1 sub2 =
        printf "(%s %s %s)" (show sub1) op (show sub2)
    in
      case formula of
        Var v -> show v
        And p q ->  pprintBinFormula "&" p q 
        Or p q ->  pprintBinFormula "|" p q 
        Impl p q -> pprintBinFormula "->" p q
        Neg p -> printf "-%s" $ show p
        Nec p ->  printf "[]%s" $ show p

instance Show Statement where
  show (Sat s formula) = show s ++" : " ++show formula
  show (Succ s s') = show s ++ " -> " ++show s'

