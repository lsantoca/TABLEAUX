import Statements
import Rules
import Tableaux
import qualified StatementsParse  as Parser
import StatementsShow

formula1 = Parser.parseString "-([](p | q) -> ([]p | []q))"
test1 =  formulaIsSatisfiable formula1

formula2 = Parser.parseString "-(([]p | []q) -> [](p | q))"
test2 =  formulaIsSatisfiable formula2


