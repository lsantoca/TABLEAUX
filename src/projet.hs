module Tableux 
import Statements
import Rules 


-- Main entry in the procedure

isSatisfiable :: Statements -> Bool
isSatisfiable statements = deduce [] statements []

deduceOneStep :: Statements -> Statement -> Statements
deduceOneStep blacks statement
  |  isUnaryRuleStatement statement = applyUnaryRules kUnaryRules statement
  |  isBinaryRuleStatement statement = applyBinaryRules kBinaryRules blacks statement
  |  otherwise = undefined

    
deduce :: Statements -> Statements -> Statements -> Bool
deduce black grey greyB
  | incoherent black grey = False
  | otherwise =
      case grey of
        [] -> branch black greyB
        st:sts ->
          if isBranchStatement st then
            deduce black sts (st:greyB)
          else 
            let
              newStatements = deduceOneStep black st
              veryNewStatements = [st | st<-newStatements,
                                   not (elem st (black ++ grey ++ greyB))  ]
            in
              deduce (st:black) (sts++veryNewStatements) greyB

branch :: Statements -> Statements -> Bool 
branch blacks [] = True
branch blacks (grey:greys) =
  let
    (statement1,statement2) = deduceBranches grey
  in
    deduce blacks [statement1] greys
    ||
    deduce blacks [statement2] greys

deduceBranches :: Statement -> (Statement,Statement)
deduceBranches statement =
  case map (\rule -> applyBranchRule rule statement) kBranchRules of
    (Just pair:_) -> pair
    _ -> error "We are not supposed to be here"
  

