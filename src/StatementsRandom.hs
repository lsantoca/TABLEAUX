module StatementsRandom where

import System.Random
import Data.List (nub)

import Statements


  
instance Random Variable where
  random g =  
    let
      (n,g') = random g
    in
      (V n,g')
  randomR (V n,V m) g =
    let
      (n,g') = randomR (n,m) g
    in
      (V n,g')

      
instance Random Formula where
  random g =
    let
      (n,g') = randomR (1,10::Int) g
    in
      case n of
        1 ->
          let
            (v,g'') = randomR (V 1 , V 10) g'
          in
            (Var v,g'')
            
            {-

instance Random Literal where
  random g =
    let
      (var,g') = random g
    in
      case randomR (1,2::Int) g' of
        (1,g'') -> (Positive var,g'')
        (2,g'') -> (Negative var,g'')

randomList 0 g = ([],g)
randomList n g =
  let
    (r,g') = random g
    (rs,g'') = randomList (n-1) g'
  in
    (r:rs,g'')

instance Random Clause where
  random g =
    let
      (n,g') = randomR (2,10::Int) g
      (literals,g'') = randomList n g'  
    in
      (Disjunction (nub literals),g'')

instance Random Clauses where
  random g =
    let
      (n,g') = randomR (20,40::Int) g
      (clauses,g'') = randomList n g'  
    in
      (SetOf (nub clauses),g'')
-}
