import Complete
import Rules
import Statements

import Control.Exception (catch,SomeException)
import System.IO.Unsafe

safeTail :: [a] -> IO [a]
safeTail xs =
  let
    failure :: SomeException -> IO [a]
    failure _ = return []
  in
    (let result = tail xs in result `seq` return result) `catch` failure

result = complete head []
state= newState
result2 = complete ruleNegNeg (Sat state (Nec (Neg (Var (V "abc")))))

{--

catchAny :: IO a -> (SomeException -> IO a) -> IO a
catchAny = Control.Exception.catch

dangerous :: IO Int
dangerous = error "Fool you!"

main :: IO ()
main = do
    result <- catchAny dangerous $ \e -> do
        putStrLn $ "Got an exception: " ++ show e
        putStrLn "Returning dummy value of -1"
        return (-1)
    print result

--}

    
