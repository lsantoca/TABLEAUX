module Complete where

import Control.Exception (catch,SomeException)
import System.IO.Unsafe

complete :: (a -> [b]) -> a -> [b]
complete f x =
  let
    failure :: SomeException -> IO [a]
    failure _ = return []
  in
    unsafePerformIO $ (let result = f x in result `seq` return result) `catch` failure

complete2 :: (a -> a-> [b]) -> a -> a -> [b]
complete2 f x y =
  let
    failure :: SomeException -> IO [a]
    failure _ = return []
  in
    unsafePerformIO $ (let result = f x y in result `seq` return result) `catch` failure
