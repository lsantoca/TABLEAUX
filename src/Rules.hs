module Rules
       {--
(
kUnaryRules,kBinaryRules,kBranchRules,
isUnaryRuleStatement,isBinaryRuleStatement,isBranchStatement,
applyUnaryRules,applyBinaryRules,applyBranchRule
)
--}
       where

import Control.Exception
import System.IO.Unsafe
import Complete
import Statements

type UnaryRule = Statement -> Statements
type UnaryRules = [UnaryRule]
--
type BinaryRule = Statement -> Statement -> Statements
type BinaryRules = [BinaryRule]
--
type BranchingRule = Statement -> [(Statement,Statement)]
type BranchingRules = [BranchingRule]

kUnaryRules :: UnaryRules
kUnaryRules = [ruleNegNeg,ruleAnd,ruleNegImpl,ruleNegOr,ruleNegNec]

ruleNegNeg,ruleAnd,ruleNegImpl,ruleNegOr, ruleNegNec :: UnaryRule
ruleNegNeg (Sat s (Neg (Neg phi))) =  [Sat s phi]
ruleAnd (Sat s (And phi psi))=  [Sat s phi, Sat s psi]
ruleNegImpl (Sat s (Neg (Impl phi psi))) = [Sat s phi, Sat s (Neg psi)]
ruleNegOr (Sat s (Neg (Or phi psi))) = [Sat s (Neg phi), Sat s (Neg psi)]
ruleNegNec (Sat s (Neg (Nec  phi))) =
  let
    s' = unsafePerformIO newState
  in
    [Sat s' (Neg phi),Succ s s']

kBinaryRules :: BinaryRules
kBinaryRules = [ruleNec]

ruleNec :: BinaryRule
ruleNec (Sat s1 (Nec phi)) (Succ s2 s3) =
  if s1 == s2 then  [Sat s3 phi] else []

kBranchRules :: BranchingRules
kBranchRules = [ruleOr,ruleNegAnd,ruleImpl]

ruleOr,ruleNegAnd,ruleImpl :: BranchingRule
ruleOr (Sat s (Or phi psi)) =  [(Sat s phi, Sat s psi)]
ruleNegAnd (Sat s (Neg (And phi psi))) =  [(Sat s (Neg phi), Sat s (Neg psi))]
ruleImpl (Sat s (Impl phi psi)) = [(Sat s (Neg phi), Sat s (Neg psi))]

applyUnaryRule :: UnaryRule -> Statement -> Statements
applyUnaryRule rule statement = complete rule statement

applyUnaryRules :: UnaryRules -> Statement -> Statements
applyUnaryRules rules statement =
  concat $ map (\r -> applyUnaryRule r statement) rules

applyBinaryRule :: BinaryRule -> Statement -> Statement -> Statements
applyBinaryRule rule statement1 statement2 =
  complete2 rule statement1 statement2
  ++
  complete2 rule statement2 statement1
  
applyBinaryRules :: BinaryRules -> Statements -> Statement -> Statements
applyBinaryRules rules statements statement =
  let
    applyToAll rule = concat $ map (applyBinaryRule rule statement) statements
  in
    concat $ map applyToAll rules

applyBranchingRule :: BranchingRule -> Statement -> [(Statement,Statement)]
applyBranchingRule rule statement = complete rule statement

applyBranchingRules :: BranchingRules -> Statement -> [(Statement,Statement)]
applyBranchingRules rules statement =
  concat $ map (\r -> applyBranchingRule r statement) rules

isUnaryRuleStatement :: Statement -> Bool
isUnaryRuleStatement statement =
  case statement of
    (Sat s (Neg (Neg _))) -> True
    (Sat s (And _ _)) -> True
    (Sat s (Neg (Impl _ _)))  -> True
    (Sat s (Neg (Or _ _))) -> True
    (Sat s (Neg (Nec  _))) -> True
    _ -> False

isBinaryRuleStatement :: Statement -> Bool
isBinaryRuleStatement (Sat s1 (Nec phi)) = True
isBinaryRuleStatement (Succ s2 s3) = True
isBinaryRuleStatement _ = False

isBranchStatement :: Statement -> Bool
isBranchStatement (Sat _ (Or _ _ )) =  True
isBranchStatement (Sat _ (Neg (And _ _))) =  True
isBranchStatement (Sat _ (Impl _ _)) = True
isBranchStatement _ = False
