module Tableaux (
  areSatisfiable -- ::  Statements -> Bool
  ,formulaIsSatisfiable -- :: Formula -> Bool
  )
       where

import Statements
import StatementsShow
import Rules 

import System.IO.Unsafe (unsafePerformIO)
import Text.Printf (printf)
import Data.List (intercalate,lines)
import Debug.Trace (trace)

statementInTraces :: Statement -> String
statementInTraces statement = printf "** %s **" (show statement)
traceString :: String -> String
traceString message =
  let
    ls= lines message
  in
    intercalate "\n" $ map ("> "++) ls

-- Main entry in the procedure

areSatisfiable :: Statements -> Bool
areSatisfiable statements
  | trace message  False = undefined
  where
    message = intercalate "\n" $ map show statements
areSatisfiable statements = deduce [] statements []

formulaIsSatisfiable :: Formula -> Bool
formulaIsSatisfiable formula =
  let
    statement =  Sat (unsafePerformIO $ newState) formula
  in
    areSatisfiable [statement]

deduceOneStep :: Statements -> Statement -> Statements
deduceOneStep _  grey
  | trace message False = undefined
  where message = traceString $ printf "Using  %s to derive." $ statementInTraces grey
deduceOneStep blacks statement
  |  isUnaryRuleStatement statement = applyUnaryRules kUnaryRules statement
  |  isBinaryRuleStatement statement = applyBinaryRules kBinaryRules blacks statement
  | isLiteral statement = []
  |  otherwise = error $ "We are in the wrong place with: "++(show statement)

deduce :: Statements -> Statements -> Statements -> Bool
--deduce black grey greyB
--  | trace ("Deducing  from the statement : " ++ show grey++"\n") False = undefined
deduce black grey greyB
  | incoherent  black grey  = False
  | otherwise =
      case grey of
        [] -> branch black greyB
        st:sts ->
          if isBranchStatement st then
            deduce black sts (st:greyB)
          else 
            let
              newStatements = deduceOneStep black st
              veryNewStatements = [st | st<-newStatements,
                                   not (elem st (black ++ grey ++ greyB))  ]
              message =
                if null veryNewStatements then
                  traceString "No new statement derived."
                else
                  intercalate "\n" $ map show veryNewStatements
            in
              trace message False
              ||
              deduce (st:black) (sts++veryNewStatements) greyB

deduceBranches :: Statement -> (Statement,Statement)
deduceBranches statement =
  case applyBranchingRules kBranchRules statement of
    (pair:_) -> pair
    _ -> error "We are not supposed to be here"

branch :: Statements -> Statements -> Bool 
branch blacks [] = True
branch blacks (grey:greys) =
  let
    (statement1,statement2) = deduceBranches grey
    message1 =
      (traceString $
      printf
      ("Incoherent branch found with statement %s.\n\n"
       ++"Backtracking to %s\nand trying second choice %s.\n"
       ++"Formulas on this branch are:"
      )
      (statementInTraces statement1) (statementInTraces grey) (statementInTraces statement2))
      ++ "\n"++
      (intercalate "\n" $ map show ((reverse blacks)++(reverse (grey:greys))))
    message2 =
      traceString $
      printf "The statement %s\nhas completely explored and leads to incoherence."
      (statementInTraces grey) 
  in
    trace (show statement1) False
    ||
    deduce blacks [statement1] greys
    ||
    trace message1 False
    ||
    trace (show statement2) False
    || 
    deduce blacks [statement2] greys
    ||
    trace message2 False
    
  

