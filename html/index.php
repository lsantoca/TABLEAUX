<?php

require_once 'classes/autoload.php';

function echoHtmlPre($string){
            echo "<html><pre>";
            echo $string;
            echo "</pre></html>";
}

class Program extends AnswerToForm {

    public $form;
    private $dir;
    private $maxlength=200;
    
    public function __construct() {
        
        $this->form = new Form('TABLEAUX', [
            'label' => "TABLEAUX pour la logique modale K",
            'submitValue'=> 'Go'
            ]);
        
        $this->form->formula = new Input('textArea','formula', [
            'label'=> "Rentrez votre formule (max $this->maxlength caractères)",
            'rows' => 2,
            'cols' => 130,
            'maxlength'=>$this->maxlength
            ]
        );
        $this->form->linkInputs();
    }

    private function executableName(){
        //$arch=php_uname("a");
        $arch=trim(shell_exec('uname -p'));
        //echo "Architecture: $arch\n";
        $indPath=dirname(__DIR__) . "/dist/runTABLEAUX_$arch";
        //echo $indPath;
        $executable = realpath($indPath);
        //echo $executable;
        if(!is_executable($executable)){
            echoHtmlPre("$executable is not executable\n");
            exit(0);
        }
        return $executable;
    }
    
    function run($formula){
        $executable=$this->executableName();
        $formulaArg = escapeshellarg($formula);
        $commandStr = trim("$executable --formula $formulaArg 2>&1");
        $command=$commandStr;
        //$command = escapeshellcmd ($commandStr);
        //echo $command;
        $output = shell_exec($command);
        echoHtmlPre($output);
    }
    
    public function execute() {
        if (!$this->isActive()) {
            return;
        }
        $this->form->fromPost();
        $formula=$this->form->formula->value;
        if(strlen($formula) > $this->maxlength){
            echoHtmlPre("Erreur : la longueur de la formule est limitée à $this->maxlength caractères");
            exit(0);
        }
        $this->run($this->form->formula->value);
        exit(0);
    }
}

$program = new Program;
$program->execute();
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="STYLESHEET" type="text/css" href="style.css" />
</head>
    <body>
    <h2>Démos de la methode des Tableaux pour la satisfaction d'une formule modale K</h2>
Syntaxe :
<ul>
<li>Conjonction : &</li>
<li>Disjunction : |</li>
<li>Implication : -></li>
<li>Negation : -</li>
<li>Necessité : []</li>
</ul> 

Exemple : [](p & q) -> []p & []q

<br />
<br />
<br />
        <?php $program->html(2); ?>
    </body>
</html>